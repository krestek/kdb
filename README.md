# kdb - MongoDB single global connection made simple
## Features
-Optimal boilerplate code minimization.

-Wraps the official MongoDB package.

## Tutorial
1. In the command line, type: **npm i kdb**
2. Add this line anywhere in the code (preferably on top level code, not inside a function): ```require('kdb').load('mongodb://username:password@ip.address:port/', 'database');```
3. You can now command your database normally (refer to MongoDB official documentation as this is a wrapper around it and works the exact same way aside from making connection easier):

```js
const db=require('kdb');

function example(){
    var cursor= db.connection('example').find();
    //etc.
}
```

## Further Info:

If you're having a problem with authentication, you can change authSource by adding it as a third parameter to the load function to solve it (Refer to official MongoDB documentation for more info):

```js
require('kdb').load('mongodb://username:password@ip.address:port', 'database', 'admin');
```
