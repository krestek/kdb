const MongoClient = require('mongodb').MongoClient;
var _db = [];

module.exports = _db;

_db.load = async (url, db, authSource) => {
    var client = await MongoClient.connect(url, { 'auth': { 'authSource': authSource ? authSource : 'admin' }, useNewUrlParser: true });
    var db = client.db(db);
    for (key in db)
        _db[key] = db[key];
    return _db;
}